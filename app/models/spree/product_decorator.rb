module Spree::ProductDecorator
  def related_products(count)
    Spree::Product.
      in_taxons(taxons).
      where.not(id: id).
      distinct.
      limit(count).
      order(:id)
  end
  Spree::Product.prepend self
end
