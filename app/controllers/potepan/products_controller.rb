class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_MAX_COUNT = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.
      related_products(RELATED_PRODUCTS_MAX_COUNT).
      includes(master: [:default_price, :images])
  end
end
