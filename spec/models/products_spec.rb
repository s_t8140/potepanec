require 'rails_helper'

RSpec.describe 'Products', type: :model do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
  
  subject { product.related_products(count) }

  describe "the number of acquisitions can be limited" do
    context "if there are 3 related products" do
      let(:count) { 3 } 
      it { expect(subject.size).to eq 3 }
    end
  
    context "if there are 4 related products" do
      let(:count) { 4 }
      it { expect(subject.size).to eq 4 }
    end
  end

  context "assign only related products" do
    let(:count) { 4 }
    it { is_expected.to match_array related_products }
  end
end