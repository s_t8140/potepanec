require 'rails_helper'

RSpec.feature "Products_features", type: :feature do
  given(:taxon_1) { create(:taxon) }
  given(:taxon_2) { create(:taxon) }
  given(:product_1) { create(:product, name: "test_bag", price: 1000, taxons: [taxon_1]) }
  given(:product_2) { create(:product, name: "test_tote", price: 2000, taxons: [taxon_2]) }
  given!(:related_products) { create_list(:product, 4, taxons: [taxon_1]) }
  
  background do
    visit potepan_product_path(product_1.id)
  end

  feature "displaying correct view" do
    scenario "verify the current path" do
      expect(current_path).to eq potepan_product_path(product_1.id)
    end

    scenario "correct display title" do
      visit potepan_product_path(product_2.id)
      expect(page).to have_title product_2.name
    end

    scenario "correct display product" do
      expect(page).to have_selector '.media-body h2', text: product_1.name
      expect(page).to have_selector '.media-body h3', text: product_1.display_price
      expect(page).to have_selector '.media-body p', text: product_1.description
    end
  end

  feature "display related_products correctly" do
    scenario "correct display related_products" do
      related_products.each do |related_product|
        expect(page).to have_selector '.productCaption h5', text: related_product.name  
        expect(page).to have_selector '.productCaption h3', text: related_product.display_price 
      end
    end
    
    scenario "do not display the same products" do
      expect(page).not_to have_selector '.productCaption h5', text: product_1.name  
      expect(page).not_to have_selector '.productCaption h3', text: product_1.display_price 
    end
    
    scenario "do not display unrelated products" do
      expect(page).not_to have_selector '.productCaption h5', text: product_2.name  
      expect(page).not_to have_selector '.productCaption h3', text: product_2.display_price 
    end

    scenario "show 4 related_products" do
      within '.productsContent' do
        expect(page).to have_selector '.productBox', count: 4
      end
    end

    context "if you have more than 5 related products" do
      given!(:related_products) {
        |i| create_list(
          :product, 5, name: "related_product_#{i}", price: "#{ rand(1.0..99.9).round(2) }", taxons: [taxon_1]
        ) 
      }
      scenario "Show only 4" do
        within '.productsContent' do
          expect(page).to have_selector '.productBox', count: 4
        end
      end
    end
  end

  feature "link verification" do
    scenario "are the category links accurate" do
      expect(page).to have_link "一覧ページへ戻る", href: potepan_category_path(product_1.taxons.first.id)
    end

    scenario "are the home links accurate" do
      expect(page).to have_link "Home", href: potepan_index_path
    end

    scenario "are the related_products links accurate" do
      related_products.each do |related_product|
        expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
        expect(page).to have_link related_product.display_price, href: potepan_product_path(related_product.id)
      end
    end
  end
end
