require 'rails_helper'

RSpec.feature "Categories_feature", type: :feature do
  given(:taxonomy) { create(:taxonomy, name: "categories") }
  given(:taxon_1) { create(:taxon, name: "taxon_1", taxonomy: taxonomy, parent: taxonomy.root) }
  given(:taxon_2) { create(:taxon, name: "taxon_2", taxonomy: taxonomy, parent: taxonomy.root) }
  given!(:product_1) { create(:product, name: "product_1", price: 1000, taxons: [taxon_1]) }
  given!(:product_2) { create(:product, name: "product_2", price: 2000, taxons: [taxon_2]) }

  background do
    visit potepan_category_path(taxonomy.root.id)
  end
  
  feature "link verification" do
    scenario "are the category links accurate" do
      expect(page).to have_link taxon_1.name, href: potepan_category_path(taxon_1.id)
      expect(page).to have_link taxon_2.name, href: potepan_category_path(taxon_2.id)
    end

    scenario "are the product_detail links accurate" do
      expect(page).to have_link product_1.name, href: potepan_product_path(product_1.id)
      expect(page).to have_link product_2.name, href: potepan_product_path(product_2.id)
    end
  end

  feature "displaying correct categoreis view" do
    scenario "verify the current path" do
      expect(current_path).to eq potepan_category_path(taxonomy.root.id)
    end

    scenario "correct display title" do
      visit potepan_category_path(taxon_1.id)
      expect(page).to have_title taxon_1.name
    end

    scenario "correct display sidebar" do
      within '.side-nav' do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon_1.name
        expect(page).to have_content taxon_1.products.count
      end
    end

    scenario "correct display products" do
      visit potepan_category_path(taxon_1.id)
      within '.productCaption' do
        expect(page).to have_content product_1.name
        expect(page).to have_content product_1.display_price
        expect(page).not_to have_content product_2.name
        expect(page).not_to have_content product_2.display_price
      end
    end
  end
end
