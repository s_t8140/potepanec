require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:product) { create(:product) }
    let(:taxonomy) { create(:taxnomy)}
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }

    before do
      get :show, params: { id: product.id }
    end

    it "response successfully" do
      expect(response).to be_successful
      expect(response).to have_http_status "200"
    end
  
    it "assigns the requested @product" do
      expect(assigns(:product)).to eq product
    end
  
    it "render show page" do
      expect(response).to render_template :show
    end
  end
  
end
