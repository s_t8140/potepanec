require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe ApplicationHelper, type: :helper do
  describe "Application Title helpers" do
    it "Title is displayed" do
      expect(full_title("test")).to eq "test - BIGBAG Store"
    end
    
    context "argument is blank" do
      it "return the correct title." do
        expect(full_title("")).to eq "BIGBAG Store"
      end
    end

    context "argument is nil" do
      it "return the correct title." do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end
  end
end
