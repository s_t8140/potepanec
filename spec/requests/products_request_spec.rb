require 'rails_helper'

RSpec.describe "Products_request", type: :request do
  describe "GET #show" do
    let(:taxon_1) { create(:taxon) }
    let(:taxon_2) { create(:taxon) }
    let(:other_taxon) { create(:taxon) }
    let!(:product) { create(:product, name: "product", price: "1000", taxons: [taxon_1, taxon_2]) }
    let!(:other_product) { create(:product, name: "other_product", price: "2000", taxons: [other_taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon_1]) }

    before do
      get potepan_product_path(product.id)
    end

    it "response successfully and 200 response" do
      expect(response).to be_successful
      expect(response).to have_http_status "200"
    end

    it "displaying the correct view" do
      expect(response).to render_template :show
    end

    it "assigns the requested instance valiables" do
      expect(assigns(:product)).to eq product
    end

    it "related products will be displayed" do
      expect(response.body).to include(related_products.first.name)
      expect(response.body).to include(related_products.first.display_price.to_s)
    end

    it "do not display unrelated products" do
      expect(response.body).not_to include "other_product"
      expect(response.body).not_to include "2000"
    end
  end
end