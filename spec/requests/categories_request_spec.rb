require 'rails_helper'

RSpec.describe "Categories_request", type: :request do
  describe "GET categories#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id, name: "taxon_1") }

    before do
      get potepan_category_path(taxon.id)
    end

    it "responce successfully and 200 response" do
      expect(response).to be_successful
      expect(response).to have_http_status "200"
    end

    it "displaying the correct view" do
      expect(response).to render_template :show
    end
  end
end